from flask import Flask, request, jsonify, abort
import socket

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def post():
	return request.get_data()
	#button used to check out which backend is being used
	# return '''<input type="button" onclick="location.href='/whoami';"
	 		#	value="whoami" />'''

@app.route('/whoami', methods=['GET','POST'])
def whoami():
    return "Hello from the backend. My name is: %s \n" % (socket.gethostname())

@app.errorhandler(404)
def page_not_found(e):
	return request.get_data()

if __name__ == '__main__':
	app.debug = False
	app.run()
